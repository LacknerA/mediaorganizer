﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaOrganizer.Modules
{
    class VideoConverter : ModuleBase
    {
        public VideoConverter(FileBrowser dirCtrl) : base(dirCtrl)
        {
            ModuleName = "Video Converter";
            Control = new VideoConverterControl(dirCtrl);
            IconPath = "../../assets/videoConverterIcon.png";
        }
        public override void ModuleSetActive()
        {
            var form = Control as ISelectionListener;

            if (form != null)
                form.EnableListening();
        }

        public override void ModuleSetInactive()
        {
            var form = Control as ISelectionListener;

            if (form != null)
                form.DisableListening();
        }
    }
}
