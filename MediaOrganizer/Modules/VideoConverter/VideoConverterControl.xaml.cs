﻿using MediaOrganizer.Components;
using MediaOrganizer.Components.Helper;
using NReco.VideoConverter;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MediaOrganizer.Modules
{
    public partial class VideoConverterControl : UserControl, INotifyPropertyChanged, ISelectionListener
    {
        #region INotifyPropertyChanged
        private void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public FileBrowser FileBrowser { get; set; }
        public IEnumerable<DirectoryItem> SelectedFiles { get; set; }

        public string OutputFileName { get; set; }
        public Dictionary<string, string> FileFormats { get; } = new Dictionary<string, string>
        {
            { Format.avi, ".avi" },
            { Format.matroska, ".mkv" },
            { Format.mp4, ".mp4" },
            { Format.flv, ".flv" },
            { "wmv", ".wmv" }
        };

        public string Status
        {
            get
            {
                return _status;
            }

            set
            {
                _status = value;
                OnPropertyChanged(nameof(Status));
            }
        }
        public bool DeleteFile { get; set; }
        public double ConvertProgress { get; set; } = 0;
        public double MaxValue { get; set; } = 1;

        private string _status;
        private bool _conversionRunning = false;

        public VideoConverterControl(FileBrowser fileBrowser)
        {
            FileBrowser = fileBrowser;

            DataContext = this;
            InitializeComponent();
        }
        public void EnableListening()
        {
            FileBrowser.FileSelectionChanged += FileBrowser_FileSelectionChanged;
            UpdateSelection();
        }

        public void DisableListening()
        {
            FileBrowser.FileSelectionChanged -= FileBrowser_FileSelectionChanged;
        }

        private async void ConvertFile(string filePath, string targetFormat, ProgressButton progress)
        {
            if (_conversionRunning){
                Status = "A conversion is already running!";
                return;
            }

            Status = "";

            if (ValidFileFormat(filePath))
            {
                var converter = new FFMpegConverter();
                var outputFile = Path.Combine(filePath.ExtractPath(), OutputFileName + FileFormats[targetFormat]);

                //Connecting convert process event
                converter.ConvertProgress += (object sender, ConvertProgressEventArgs args) =>
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        SetProgressBar(args.Processed.TotalSeconds, args.TotalDuration.TotalSeconds);                        
                    });
                };

                progress.ShowProgress();
                _conversionRunning = true;

                //Run conversion async
                await Task.Run(() =>
                {
                    try
                    {
                        converter.ConvertMedia(filePath, outputFile, targetFormat);
                    }
                    catch (Exception ex)
                    {
                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            Status = ex.Message;
                        });
                    }
                });

                _conversionRunning = false;
                progress.HideProgress();

                if (DeleteFile)
                    File.Delete(filePath);

                FileBrowser.DirectoryController.LoadDirectory(false);
                SetProgressBar(0, 1);

                Status = "File successfully converted to '" + outputFile.GetFileName() + "'";
            }
            else
            {
                Status = "The selected item is not valid!";
            }
        }

        private bool ValidFileFormat(string filePath)
        {
            return FileFormats.Values.Contains(filePath.GetFileExtension());
        }

        private void SetProgressBar(double progress, double maxValue)
        {
            MaxValue = maxValue;
            ConvertProgress = progress;

            OnPropertyChanged(nameof(MaxValue));
            OnPropertyChanged(nameof(ConvertProgress));
        }

        private void SetFileName()
        {
            var firstSelected = SelectedFiles.FirstOrDefault(f => f.GetType() == typeof(FileObject));

            if (firstSelected != null)
                OutputFileName = firstSelected.ItemName.RemoveFileExtension();

            OnPropertyChanged(nameof(OutputFileName));
        }

        private void UpdateSelection()
        {
            SelectedFiles = from file in FileBrowser.DirectoryController.DirectoryContent
                            where file.IsSelected
                            select file;

            SetFileName();
        }

        private void FileBrowser_FileSelectionChanged()
        {
            UpdateSelection();
        }

        #region UIEvents
        private void ConvertAvi_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as ProgressButton;
            var firstSelected = SelectedFiles?.FirstOrDefault(f => f?.GetType().Equals(typeof(FileObject)) ?? false);

            if (firstSelected != null)
                ConvertFile(firstSelected.FullPath, Format.avi, btn);
        }

        private void ConvertMkv_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as ProgressButton;
            var firstSelected = SelectedFiles?.FirstOrDefault(f => f?.GetType().Equals(typeof(FileObject)) ?? false);

            if (firstSelected != null)
                ConvertFile(firstSelected.FullPath, Format.matroska, btn);
        }

        private void ConvertMp4_Click(object sender, RoutedEventArgs e)
        {
            var btn = sender as ProgressButton;
            var firstSelected = SelectedFiles?.FirstOrDefault(f => f?.GetType().Equals(typeof(FileObject)) ?? false);

            if (firstSelected != null)
                ConvertFile(firstSelected.FullPath, Format.mp4, btn);
        }
        #endregion
    }
}
