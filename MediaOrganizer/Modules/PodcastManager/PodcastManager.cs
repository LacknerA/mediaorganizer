﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MediaOrganizer.Modules
{
    class PodcastManager : ModuleBase, INavigable
    {
        private Stack<UserControl> navigationStack = new Stack<UserControl>();

        public PodcastManager(FileBrowser dirCtrl) : base(dirCtrl)
        {
            ModuleName = "Podcast Manager";
            Control = new PodcastManagerControl(dirCtrl, this);
            IconPath = "../../assets/podcastManagerIcon.png";
        }

        public void NavigateTo(UserControl ctrl)
        {
            navigationStack.Push(Control);
            Control = ctrl;
        }

        public void NavigateBack()
        {
            Control = navigationStack.Pop();
        }
    }
}
