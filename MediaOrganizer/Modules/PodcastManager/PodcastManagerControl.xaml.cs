﻿using MediaOrganizer.Components.Helper;
using PodcastLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace MediaOrganizer.Modules
{
    public partial class PodcastManagerControl : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        private void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        private const string PodcastStorage = "podcasts.mcfg";

        public FileBrowser FileBrowser { get; set; }
        public INavigable DisplayContainer { get; set; }
        public string PodcastRootPath { get; set; }

        public ObservableCollection<PodcastContainer> Podcasts { get; set; } = new ObservableCollection<PodcastContainer>();
        public PodcastContainer SelectedPodcast { get; set; }
        public ActionCommand<PodcastContainer> TrashPodcast { get; set; }

        public bool ValidRoot
        {
            get
            {
                return PodcastRootPath != null;
            }
        }

        private PodcastEditControl _podcastEdit;
        
        public PodcastManagerControl(FileBrowser fileBrowser)
        {
            FileBrowser = fileBrowser;

            // Initialize trash command
            TrashPodcast = new ActionCommand<PodcastContainer>((PodcastContainer podcast) =>
            {
                RemovePodcast(podcast);
            });

            _podcastEdit = new PodcastEditControl(fileBrowser);
            _podcastEdit.EditFinished += PodcastEditFinished;

            DataContext = this;
            InitializeComponent();
        }

        public PodcastManagerControl(FileBrowser fileBrowser, INavigable navigable) : this(fileBrowser)
        {
            DisplayContainer = navigable;
        }

        public void LoadPodcasts()
        {
            var storagePath = Path.Combine(PodcastRootPath, PodcastStorage);

            if (File.Exists(storagePath))
            {
                Podcasts = new ObservableCollection<PodcastContainer>(
                    PersistenceManager.ReadFromXml<ObservableCollection<PodcastContainer>>(storagePath)
                    .OrderBy(rc => rc.Channel.Title, new NaturalSortComparator()));

                if(Podcasts == null)
                    Podcasts = new ObservableCollection<PodcastContainer>();

                OnPropertyChanged(nameof(Podcasts));
            }
            else
            {
                Podcasts = new ObservableCollection<PodcastContainer>();
            }
        }

        public void AddPodcast()
        {
            if (Podcasts?.Count == 0)
                InitializeRoot();

            var pc = new PodcastContainer();
            pc.RootDirectory = PodcastRootPath;
            
            _podcastEdit.Podcast = pc;
            DisplayContainer.NavigateTo(_podcastEdit);
        }

        public void RemovePodcast(PodcastContainer podcast)
        {
            if(podcast != null)
            {
                var result = MessageBox.Show($"Do you really want to delete \"{podcast.Channel.Title}\"?", "Delete Podcast", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if(result == MessageBoxResult.Yes)
                {
                    Podcasts.Remove(podcast);
                    SavePodcasts();
                }
            }
        }

        public void SavePodcasts()
        {
            if (!Directory.Exists(PodcastRootPath))
                throw new Exception("Defined root directory does not exist!");

            var storagePath = Path.Combine(PodcastRootPath, PodcastStorage);
            PersistenceManager.WriteToXml(Podcasts, storagePath);
        }

        public void CleanPodcastDirectory()
        {
            if (!Directory.Exists(PodcastRootPath))
                throw new Exception("Defined root directory does not exist!");

            try
            {
                int removedItems = 0;
                var imgPath = Path.Combine(PodcastRootPath, "img");
                var imgDir = new DirectoryInfo(imgPath);

                foreach (var img in imgDir.GetFiles())
                {
                    if (!ImageUsed(img.Name))
                    {
                        File.Delete(img.FullName);
                        removedItems++;
                    }
                }

                MessageBox.Show($"Directory {PodcastRootPath} cleaned!\n{removedItems} files were deleted.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception)
            {
                MessageBox.Show($"Cleanup failed!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public void GeneratePodcast()
        {
            if (SelectedPodcast == null) return;

            var gen = new RssGenerator();
            var genTitle = SelectedPodcast.Channel.Title + ".xml";
            var genPath = Path.Combine(PodcastRootPath, genTitle);

            gen.Generate(genPath, SelectedPodcast.Channel);

            MessageBox.Show($"Podcast {SelectedPodcast.Channel.Title} generated!", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
        }
        
        private void InitializeRoot()
        {
            if (!Directory.Exists(PodcastRootPath))
                throw new Exception("Defined root directory does not exist!");

            // Create the image directory
            var imgDirectory = Path.Combine(PodcastRootPath, "img");
            Directory.CreateDirectory(imgDirectory);
        }

        private void PodcastEditFinished(PodcastContainer podcast, EditResult result)
        {
            if(result == EditResult.Save)
            {
                if (!Podcasts.Contains(podcast))
                    Podcasts.Add(podcast);
                
                SavePodcasts();
                LoadPodcasts();
            }

            DisplayContainer.NavigateBack();
        }

        private bool ImageUsed(string name)
        {
            foreach(var pc in Podcasts)
            {
                if (pc.Channel.Image.GetFileName() == name)
                    return true;

                foreach (var feedItem in pc.Channel.Items.Cast<PodcastItem>())
                    if (feedItem.Image.GetFileName() == name)
                        return true;
            }

            return false;
        }


        #region UIEvents
        private void SelectPodcastRoot_Click(object sender, RoutedEventArgs e)
        {
            var folderBrowser = new System.Windows.Forms.FolderBrowserDialog();
            var res = folderBrowser.ShowDialog();

            if (res == System.Windows.Forms.DialogResult.OK)
            {
                PodcastRootPath = folderBrowser.SelectedPath;

                LoadPodcasts();

                OnPropertyChanged(nameof(PodcastRootPath));
                OnPropertyChanged(nameof(ValidRoot));
            }
        }
        
        private void GeneratePodcast_Click(object sender, RoutedEventArgs e)
        {
            GeneratePodcast();
        }

        private void ReloadPodcasts_Click(object sender, RoutedEventArgs e)
        {
            LoadPodcasts();
        }

        private void AddPodcast_Click(object sender, RoutedEventArgs e)
        {
            AddPodcast();
        }

        private void CleanDirectory_Click(object sender, RoutedEventArgs e)
        {
            CleanPodcastDirectory();
        }

        private void CopyUrl_Click(object sender, RoutedEventArgs e)
        {
            if(SelectedPodcast != null)
            {
                var url = SelectedPodcast.RootDomain + (SelectedPodcast.RootDomain.EndsWith("/") ? "": "/");
                url += SelectedPodcast.Channel.Title + ".xml";

                url = url.ToUrl();

                Clipboard.SetText(url);
                MessageBox.Show($"Podcast url was copied to clipboard!\n{ url }", "Copied to clipboard", MessageBoxButton.OK, MessageBoxImage.Information);
            }
        }

        private void Channel_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if(e.ClickCount > 1)
            {
                _podcastEdit.Podcast = SelectedPodcast;
                DisplayContainer.NavigateTo(_podcastEdit);
            }
        }
        #endregion
    }
}
