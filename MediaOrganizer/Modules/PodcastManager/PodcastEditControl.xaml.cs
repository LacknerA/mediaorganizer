﻿using MediaOrganizer.Components.Helper;
using Microsoft.Win32;
using PodcastLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace MediaOrganizer.Modules
{
    public enum EditResult
    {
        Save,
        Canceled
    }

    public partial class PodcastEditControl : UserControl, INotifyPropertyChanged, ISelectionListener
    {
        #region INotifyPropertyChanged
        private void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public event Action<PodcastContainer, EditResult> EditFinished;

        public FileBrowser FileBrowser { get; set; }
        public IEnumerable<DirectoryItem> SelectedFiles { get; set; }

        private PodcastContainer _podcast;
        public PodcastContainer Podcast
        {
            get
            {
                return _podcast;
            }
            set
            {
                _podcast = value;
                OnPropertyChanged(nameof(Podcast));
            }
        }

        public ActionCommand<PodcastItem> TrashPodcastItem { get; set; }

        public PodcastEditControl(FileBrowser fileBrowser)
        {
            FileBrowser = fileBrowser;

            // Initialize remove command
            TrashPodcastItem = new ActionCommand<PodcastItem>((PodcastItem podcast) =>
            {
                RemovePodcastItem(podcast);
            });

            DataContext = this;
            InitializeComponent();
        }

        public void SetPodcastImage()
        {
            var imageSelect = new OpenFileDialog();
            imageSelect.Filter = "Image files (*.png;*.jpeg;*.jpg)|*.png;*.jpeg;*.jpg|All files (*.*)|*.*";

            if (imageSelect.ShowDialog() == true)
            {
                var imgName = Guid.NewGuid().ToString() + imageSelect.FileName.GetFileExtension();
                var podcastDirectory = Path.Combine(Podcast.RootDirectory, "img");
                var imgTarget = Path.Combine(podcastDirectory, imgName);

                // Copy image to correct location
                File.Copy(imageSelect.FileName, imgTarget);

                // Set link
                Podcast.Channel.Image = Path.Combine(Podcast.RootDomain, "img", imgName).ToValidPath();

                OnPropertyChanged(nameof(Podcast));
            }

        }

        public async void LoadPodcastItems()
        {
            var activePath = FileBrowser.DirectoryController.CurrentPath;

            Mouse.OverrideCursor = Cursors.Wait;

            try
            {
                int itemIndex = 0;
                var podcastItems = new List<PodcastItem>();

                await Task.Run(() =>
                {
                    // Search files
                    foreach (var item in FileBrowser.DirectoryController.GetMatches(activePath, "*.mp3"))
                    {
                        var relItemPath = item.FullName.Substring(activePath.Length);
                        var meta = TagLib.File.Create(item.FullName);
                        var pi = CreatePodcastItem(meta, relItemPath);
                        
                        podcastItems.Add(pi);
                    }

                });

                // Sort and set items
                Podcast.Channel.Items = new List<FeedItem>(podcastItems
                    .OrderBy(i => i.Title, new NaturalSortComparator()));

                // Reduce publication date to order podcasts
                foreach (var feedItem in Podcast.Channel.Items.Cast<PodcastItem>())
                {
                    feedItem.PublicationDate = feedItem.PublicationDate.AddDays((itemIndex++) * -1);
                }

                OnPropertyChanged(nameof(Podcast));
                PodcastItemListView.Items.Refresh();
            }
            catch (Exception)
            {
                MessageBox.Show("Could not load Path!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }

            Mouse.OverrideCursor = Cursors.Arrow;
        }

        private PodcastItem CreatePodcastItem(TagLib.File meta, string relItemPath)
        {
            var pi = new PodcastItem();
            var url = (Podcast.DataPath + relItemPath.ToValidPath()).ToUrl();

            // Image
            var imgName = Guid.NewGuid().ToString() + ".jpg";
            var imgUrl = Path.Combine(Podcast.RootDomain, "img", imgName).ToValidPath();
            var imgPath = Path.Combine(Podcast.RootDirectory, "img", imgName).ToValidPath();
            
            SaveAsImage(meta.Tag.Pictures.FirstOrDefault(), imgPath);

            // Meta data
            pi.Title = meta.Tag.Title;
            pi.Author = meta.Tag.JoinedPerformers;
            pi.PublicationDate = DateTime.Now;
            pi.Duration = (int)meta.Properties.Duration.TotalSeconds;
            pi.Explicit = false;
            pi.Image = imgUrl;
            pi.Guid = url;

            pi.Enclosure.Length = (int)meta.Properties.Duration.TotalSeconds;
            pi.Enclosure.Type = "audio/mpeg";
            pi.Enclosure.Url = url;

            return pi;
        }

        private void RemovePodcastItem(PodcastItem pi)
        {
            Podcast?.Channel?.Items?.Remove(pi);

            OnPropertyChanged(nameof(Podcast));
            PodcastItemListView.Items.Refresh();
        }

        private void SaveAsImage(TagLib.IPicture picture, string path)
        {
            if (picture == null) return;

            MemoryStream ms = new MemoryStream(picture.Data.Data);
            System.Drawing.Image image = System.Drawing.Image.FromStream(ms);

            image.Save(path);
        }
                
        #region FileSelection
        public void EnableListening()
        {
            FileBrowser.FileSelectionChanged += FileBrowser_FileSelectionChanged;
            UpdateSelection();
        }

        public void DisableListening()
        {
            FileBrowser.FileSelectionChanged -= FileBrowser_FileSelectionChanged;
        }

        private void UpdateSelection()
        {
            SelectedFiles = from file in FileBrowser.DirectoryController.DirectoryContent
                            where file.IsSelected
                            select file;

        }

        private void FileBrowser_FileSelectionChanged()
        {
            UpdateSelection();
        }
        #endregion

        #region UIEvents
        private void Save_Click(object sender, RoutedEventArgs e)
        {
            EditFinished(Podcast, EditResult.Save);
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            EditFinished(Podcast, EditResult.Canceled);
        }

        private void SelectImage_Click(object sender, RoutedEventArgs e)
        {
            SetPodcastImage();
        }

        private void LoadItems_Click(object sender, RoutedEventArgs e)
        {
            LoadPodcastItems();
        }
        #endregion
    }
}
