﻿using PodcastLib;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaOrganizer.Modules
{
    public class PodcastContainer
    {
        public PodcastChannel Channel { get; set; }

        public string RootDirectory { get; set; } = "";
        public string RootDomain { get; set; } = "";
        public string DataPath { get; set; } = "";

        public PodcastContainer()
        {
            Channel = new PodcastChannel();

            Channel.Language = "de-DE";
            Channel.Category.Category = "Music";
            Channel.Explicit = false;
        }
    }
}
