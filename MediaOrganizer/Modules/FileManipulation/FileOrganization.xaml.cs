﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MediaOrganizer.Modules
{
    public partial class FileOrganization : UserControl
    {
        public FileOrganization()
        {
            InitializeComponent();
        }

        #region UIEvents
            private void FileBackButton_Click(object sender, RoutedEventArgs e)
            {
                FileBrowser.DirectoryController.GoBack();
            }

            private void FileForwardButton_Click(object sender, RoutedEventArgs e)
            {
                FileBrowser.DirectoryController.GoForward();
            }
        #endregion
    }
}
