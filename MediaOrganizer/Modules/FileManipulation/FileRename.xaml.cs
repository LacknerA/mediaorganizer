﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.IO;
using MediaOrganizer.Components.Helper;
using System;

namespace MediaOrganizer.Modules
{
    public partial class FileRename : INotifyPropertyChanged, ISelectionListener
    {
        private struct RenameAction
        {
            public string Path { get; set; }
            public string OldName { get; set; }
            public string NewName { get; set; }

            public Func<string, bool> Exists;
            public Action<string, string> Move;
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        public FileBrowser FileBrowser { get; set; }
        public IEnumerable<DirectoryItem> SelectedFiles { get; set; }

        #region UIProperties
        public string Status { get; set; }
        public int SelectionCount
        {
            get
            {
                if (SelectedFiles != null)
                    return SelectedFiles.Count();

                return 0;
            }
        }
        
        public string FileNamePattern { get; set; } = "";
        public string RenamePlaceholder { get; set; } = "<index>";
        public int RenameStartIndex { get; set; } = 1;
        public int IndexWidth { get; set; } = 2;
        public IEnumerable<string> RenamePreview { get; set; }
        public bool RenameEnabled => (SelectionCount > 0);

        private Stack<List<RenameAction>> _renameStack;
        private bool _refreshStatus;
        #endregion

        public FileRename(FileBrowser fileBrowser)
        {
            FileBrowser = fileBrowser;

            _renameStack = new Stack<List<RenameAction>>();
            _refreshStatus = true;

            UpdateSelection();

            DataContext = this;
            InitializeComponent();
        }

        public void EnableListening()
        {
            FileBrowser.FileSelectionChanged += FileBrowser_FileSelectionChanged;
            UpdateSelection();
        }

        public void DisableListening()
        {
            FileBrowser.FileSelectionChanged -= FileBrowser_FileSelectionChanged;
        }

        public void Rename()
        {
            if (SelectedFiles.All(sf => sf.GetType() == typeof(FileObject)) || SelectedFiles.All(sf => sf.GetType() == typeof(DirectoryObject)))
            {
                var index = RenameStartIndex;
                var renameActions = new List<RenameAction>();

                foreach (var item in SelectedFiles)
                {
                    var action = new RenameAction();

                    action.OldName = item.ItemName;
                    action.NewName = BuildFileName(item.ItemName, index);
                    action.Path = item.FullPath.ExtractPath();

                    action.Exists = item.Exists;
                    action.Move = item.Move;

                    if (!item.Exists(Path.Combine(action.Path, action.NewName)))
                    {
                        item.Move(item.FullPath, Path.Combine(action.Path, action.NewName));
                        renameActions.Add(action);
                    }
                    else
                    {
                        Status = "One or more items could not be renamed because a items with this name already exists.";
                    }

                    index++;
                }

                _renameStack.Push(renameActions);

                _refreshStatus = false;
                FileBrowser.DirectoryController.LoadDirectory(false);
                _refreshStatus = true;
            }
            else
            {
                Status = "Please select only files or directories!";
            }
        }

        public void UndoRename()
        {
            if (_renameStack.Count > 0)
            {
                var renameActions = _renameStack.Pop();

                foreach (var action in renameActions)
                {
                    if (!action.Exists(Path.Combine(action.Path, action.OldName)))
                    {
                        action.Move(Path.Combine(action.Path, action.NewName), Path.Combine(action.Path, action.OldName));
                    }
                }

                _refreshStatus = false;
                FileBrowser.DirectoryController.LoadDirectory(false);
                _refreshStatus = true;
            }
        }

        public void UpdateRenamePreview()
        {
            if (SelectedFiles.All(sf => sf.GetType() == typeof(FileObject)) || SelectedFiles.All(sf => sf.GetType() == typeof(DirectoryObject)))
            {
                if (_refreshStatus)
                {
                    if (SelectedFiles.Count() > 1 && !FileNamePattern.Contains(RenamePlaceholder))
                        Status = "You have multiple files selected. Please use the placeholder to create unique filenames.";
                    else
                        Status = "";
                }

                var renamePreview = new List<string>();
                int index = RenameStartIndex;

                foreach (var file in SelectedFiles)
                {
                    renamePreview.Add(BuildFileName(file.ItemName, index));
                    index++;
                }

                RenamePreview = renamePreview;
            }
            else
            {
                RenamePreview = null;
            }
        }
        
        public void UpdateNamePattern()
        {
            if (SelectedFiles.All(sf => sf.GetType() == typeof(FileObject)) || SelectedFiles.All(sf => sf.GetType() == typeof(DirectoryObject)))
            {
                FileNamePattern = SelectedFiles.FirstOrDefault()?.ItemName.RemoveFileExtension();
            }
        }

        public string BuildFileName(string fileName, int index)
        {
            var fileType = Path.GetExtension(fileName);
            var displayIndex = index.ToString().PadLeft(IndexWidth > 0 ? IndexWidth : 0, '0');

            //Replace index
            var name = FileNamePattern.Replace(RenamePlaceholder, displayIndex) + fileType;

            //Replace tags
            name = TextExtractor.ReplaceExtractorTags(name, fileName);

            return name;
        }

        private void UpdateSelection()
        {
            SelectedFiles = from file in FileBrowser.DirectoryController.DirectoryContent
                            where file.IsSelected
                            select file;

            UpdateNamePattern();
            UpdateRenamePreview();

            NotifyValuesChanged();
        }

        private void NotifyValuesChanged()
        {
            OnPropertyChanged(nameof(Status));
            OnPropertyChanged(nameof(SelectionCount));
            OnPropertyChanged(nameof(FileNamePattern));
            OnPropertyChanged(nameof(RenameEnabled));
            OnPropertyChanged(nameof(RenamePreview));
        }

        private void FileBrowser_FileSelectionChanged()
        {
            UpdateSelection();
        }

        #region UIEvents
        private void RenameButton_Click(object sender, RoutedEventArgs e)
        {
            Rename();
            NotifyValuesChanged();
        }

        private void UndoRename_Click(object sender, RoutedEventArgs e)
        {
            UndoRename();
            NotifyValuesChanged();
        }

        private void RenamePattern_TextChanged(object sender, TextChangedEventArgs e)
        {
            UpdateRenamePreview();
            OnPropertyChanged(nameof(RenamePreview));
            OnPropertyChanged(nameof(Status));
        }

        private void RenamePattern_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                Rename();
        }

        private void RenamePlaceholder_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            var caretPos = FilePatternInput.CaretIndex;
    
            FileNamePattern = FileNamePattern.Substring(0, caretPos) +
                RenamePlaceholder +
                FileNamePattern.Substring(caretPos);

            OnPropertyChanged(nameof(FileNamePattern));
            FilePatternInput.CaretIndex = caretPos + RenamePlaceholder.Length;
        }

        private void NumberSelector_ValueChanged(object sender, RoutedEventArgs e)
        {
            UpdateRenamePreview();
            NotifyValuesChanged();
        }
        #endregion
    }

}
