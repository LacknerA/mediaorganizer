﻿using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using MediaOrganizer.Components.Helper;
using System;

namespace MediaOrganizer.Modules
{
    public partial class DateTagger : UserControl, INotifyPropertyChanged, ISelectionListener
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public FileBrowser FileBrowser { get; set; }
        public IEnumerable<DirectoryItem> SelectedFiles { get; set; }

        public IEnumerable<string> DateSource { get; set; }
        public bool ApplySub { get; set; }

        public DateTagger(FileBrowser fileBrowser)
        {
            FileBrowser = fileBrowser;

            FillDateSource();
            UpdateSelection();

            DataContext = this;
            InitializeComponent();
        }

        public void EnableListening()
        {
            FileBrowser.FileSelectionChanged += FileBrowser_FileSelectionChanged;
            UpdateSelection();
        }

        public void DisableListening()
        {
            FileBrowser.FileSelectionChanged -= FileBrowser_FileSelectionChanged;
        }

        private void ApplyDate(string date)
        {
            var item = SelectedFiles?.FirstOrDefault(di => di.GetType() == typeof(FileObject) || di.GetType() == typeof(DirectoryObject));

            if (item != null)
            {
                var name = item.ItemName.RemoveFileExtension() + " (" + date + ")" + item.ItemName.GetFileExtension();
                item.RenameItem(Path.Combine(item.FullPath.ExtractPath(), name));

                if (item.GetType() == typeof (DirectoryObject))
                {
                    var dirInf = new DirectoryInfo(item.FullPath);
                    foreach (var file in dirInf.GetFiles())
                    {
                        var fname = file.Name.RemoveFileExtension() + " (" + date + ")" + file.Name.GetFileExtension();
                        File.Move(file.FullName, Path.Combine(item.FullPath, fname));
                    }
                }

                FileBrowser.DirectoryController.LoadDirectory(false);
            }
        }

        private void FillDateSource()
        {
            var dates = new List<string>();

            for (int i = 1995; i < 2020; i++)
                dates.Add(i.ToString());

            DateSource = dates;
        }

        private void UpdateSelection()
        {
            SelectedFiles = from file in FileBrowser.DirectoryController.DirectoryContent
                            where file.IsSelected
                            select file;
        }

        private void FileBrowser_FileSelectionChanged()
        {
            UpdateSelection();
        }

        private void Tag_Click(object sender, MouseButtonEventArgs e)
        {
            var date = (sender as TextBlock)?.Text;

            if (date != null)
                ApplyDate(date);
        }
    }
}
