﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using MediaOrganizer.Components.Helper;

namespace MediaOrganizer.Modules
{
    public partial class NameExtension : UserControl, INotifyPropertyChanged, ISelectionListener
    {
        public class ExtensionContainer : INotifyPropertyChanged
        {
            #region INotifyPropertyChanged
            private void OnPropertyChanged(string on)
            {
                if (PropertyChanged == null) return;
                PropertyChanged(this, new PropertyChangedEventArgs(on));
            }
            public event PropertyChangedEventHandler PropertyChanged;
            #endregion

            public string FullName { get; set; }

            private string _existingName = string.Empty;
            public string ExistingName
            {
                get
                {
                    return _existingName;
                }

                set
                {
                    _existingName = value;
                    OnPropertyChanged(nameof(ExistingName));
                }
            }

            private string _extension = string.Empty;
            public string Extension
            {
                get
                {
                    return _extension;
                }

                set
                {
                    _extension = value;
                    OnPropertyChanged(nameof(Extension));
                }
            }
        }

        #region INotifyPropertyChanged
        private void OnPropertyChanged(string on)
        {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(on));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public FileBrowser FileBrowser { get; set; }
        public List<ExtensionContainer> SelectedFiles { get; set; }
        public ActionCommand<ExtensionContainer> PasteCommand { get; set; }


        public NameExtension(FileBrowser _fileBrowser)
        {
            FileBrowser = _fileBrowser;

            PasteCommand = new ActionCommand<ExtensionContainer>(HandlePaste);

            DataContext = this;
            InitializeComponent();
        }

        public void EnableListening()
        {
            FileBrowser.FileSelectionChanged += FileBrowser_FileSelectionChanged;
            UpdateSelection();
        }

        public void DisableListening()
        {
            FileBrowser.FileSelectionChanged -= FileBrowser_FileSelectionChanged;
        }

        public void ApplyExtension()
        {
            if (SelectedFiles.Any(sf => sf.Extension != string.Empty))
            {
                foreach (var ext in SelectedFiles)
                {
                    var extName = ext.ExistingName.RemoveFileExtension() + ext.Extension + ext.ExistingName.GetFileExtension();

                    if (!File.Exists(Path.Combine(ext.FullName.ExtractPath(), extName)))
                    {
                        File.Move(ext.FullName, Path.Combine(ext.FullName.ExtractPath(), extName));
                    }
                }

                ClearExtension();
                FileBrowser.DirectoryController.LoadDirectory(false);
            }
        }

        public void ClearExtension()
        {
            foreach (var ext in SelectedFiles)
                ext.Extension = "";
        }

        private void UpdateSelection()
        {
            SelectedFiles = (from file in FileBrowser.DirectoryController.DirectoryContent
                             where file.IsSelected
                             select new ExtensionContainer()
                             {
                                 FullName = file.FullPath,
                                 ExistingName = file.ItemName,
                                 Extension = ""
                             }).ToList();

            OnPropertyChanged(nameof(SelectedFiles));
        }

        private void HandlePaste(ExtensionContainer sender)
        {
            var clipboard = Clipboard.GetData(DataFormats.Text) as String;

            if (clipboard != null)
            {
                var data = Regex.Split(clipboard, "\r\n|\r|\n");
                bool pasteData = false;
                int i = 0;

                foreach (var ext in SelectedFiles)
                {
                    if (pasteData || sender == ext)
                    {
                        pasteData = true;

                        if (i < data.Length)
                            ext.Extension = data[i++];
                    }
                }

                OnPropertyChanged(nameof(SelectedFiles));
            }
        }

        private void FileBrowser_FileSelectionChanged()
        {
            UpdateSelection();
        }

        #region UIEvents
        private void Apply_Click(object sender, RoutedEventArgs e)
        {
            ApplyExtension();
        }

        private void Clear_Click(object sender, RoutedEventArgs e)
        {
            ClearExtension();
        }
        #endregion
    }
}
