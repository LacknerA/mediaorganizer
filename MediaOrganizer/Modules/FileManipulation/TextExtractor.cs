﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MediaOrganizer.Modules
{
    class TextExtractor
    {
        private struct Tag
        {
            public string Origin { get; set; }
            public string RegEx { get; set; }
            public IEnumerable<ReplacementRule> Replacements { get; set; }      
        }

        private struct ReplacementRule
        {
            public string Source { get; set; }
            public string Replacement { get; set; }
        }

        public static string ReplaceExtractorTags(string input, string source)
        {
            var tags = GetTags(input);

            foreach(var tag in tags)
            {   
                try {                 
                    var match = Regex.Match(source, tag.RegEx).Groups[1].Value;

                    //Apply rename rules
                    foreach (var rule in tag.Replacements)
                        match = match.Replace(rule.Source, rule.Replacement);

                    input = input.Replace(tag.Origin, match);
                }
                catch { }
            }

            return input;
        }

        private static IEnumerable<Tag> GetTags(string input)
        {
            var tags = new List<Tag>();
            var matches = Regex.Matches(input, @"\?\{(?<regex>[^}]*)\}");

            foreach (Match match in matches)
            {
                var tag = new Tag
                {
                    Origin = match.Groups[0].Value,
                    RegEx = GetRegEx(match.Groups[1].Value),
                    Replacements = GetReplacements(match.Groups[1].Value)
                };
                
                tags.Add(tag);
            }

            return tags;
        }

        private static string GetRegEx(string tag)
        {
            var match = Regex.Match(tag, "^(?<regex>[^,]*),");
            var result = match.Groups["regex"].Value;

            if (!string.IsNullOrEmpty(result))
                return result;

            return tag;
        }

        private static IEnumerable<ReplacementRule> GetReplacements(string tag)
        {
            var replacements = new List<ReplacementRule>();
            var matches = Regex.Matches(tag, @"'(?<source>.*)'.*=.*'(?<replacement>.*)'");

            foreach (Match match in matches)
            {
                var replacement = new ReplacementRule
                {
                    Source = match.Groups["source"].Value,
                    Replacement = match.Groups["replacement"].Value
                };

                replacements.Add(replacement);
            }

            return replacements;
        }
    }
}
