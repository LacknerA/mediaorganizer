﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaOrganizer.Modules
{
    class FileManipulation : ModuleBase
    {
        public FileManipulation(FileBrowser dirCtrl) : base(dirCtrl)
        {
            ModuleName = "File Manipulation";
            Control = new FileManipulationControl(dirCtrl);
            IconPath = "../../assets/fileManipulationIcon.png";
        }

        public override void ModuleSetActive()
        {
            var form = Control as ISelectionListener;

            if (form != null)
                form.EnableListening();
        }

        public override void ModuleSetInactive()
        {
            var form = Control as ISelectionListener;

            if (form != null)
                form.DisableListening();
        }
    }
}
