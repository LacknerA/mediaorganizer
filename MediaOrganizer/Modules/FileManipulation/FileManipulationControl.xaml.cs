﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace MediaOrganizer.Modules
{
    public partial class FileManipulationControl : UserControl, ISelectionListener
    {
        public FileBrowser FileBrowser { get; set; }

        public UserControl FileRename { get; set; }
        public UserControl NameExtension { get; set; }
        public UserControl FileOrganizer { get; set; }
        public UserControl DateTagger { get; set; }

        public FileManipulationControl(FileBrowser fileBrowser)
        {
            FileBrowser = fileBrowser;

            FileRename = new FileRename(FileBrowser);
            NameExtension = new NameExtension(FileBrowser);
            DateTagger = new DateTagger(FileBrowser);
            FileOrganizer = new FileOrganization();

            DataContext = this;
            InitializeComponent();
        }

        public void EnableListening()
        {
            (FileRename as ISelectionListener)?.EnableListening();
            (NameExtension as ISelectionListener)?.EnableListening();
            (DateTagger as ISelectionListener)?.EnableListening();
        }

        public void DisableListening()
        {
            (FileRename as ISelectionListener)?.DisableListening();
            (NameExtension as ISelectionListener)?.DisableListening();
            (DateTagger as ISelectionListener)?.DisableListening();
        }
    }
}
