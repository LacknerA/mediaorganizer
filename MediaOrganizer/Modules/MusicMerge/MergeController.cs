﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaOrganizer.Modules
{
    public class MergeController
    {
        public ObservableCollection<MergeJob> MergeJobs { get; set; }

        public MergeController()
        {
            MergeJobs = new ObservableCollection<MergeJob>();
        }

        public async void ExecuteJob(MergeJob mj)
        {
            MergeJobs.Add(mj);

            await mj.ProcessJobAsync();

            MergeJobs.Remove(mj);
        } 
    }
}
