﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaOrganizer.Modules
{
    class MusicMerge : ModuleBase
    {
        public MusicMerge(FileBrowser dirCtrl) : base(dirCtrl)
        {
            ModuleName = "Music Merge";
            Control = new MusicMergeControl(dirCtrl);
            IconPath = "../../assets/musicMergeIcon.png";
        }

        public override void ModuleSetActive()
        {
            var form = Control as ISelectionListener;

            if (form != null)
                form.EnableListening();
        }

        public override void ModuleSetInactive()
        {
            var form = Control as ISelectionListener;

            if (form != null)
                form.DisableListening();
        }
    }
}
