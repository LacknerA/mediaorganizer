﻿using MediaOrganizer.Components.Helper;
using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace MediaOrganizer.Modules
{
    public class MergeJob : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        private void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public event Action MergeJobFinished;

        public List<DirectoryItem> SourceFiles { get; set; }
        public string TargetFile { get; set; }
        public string TargetName
        {
            get
            {
                return TargetFile.GetFileName();
            }
        }

        public bool DeleteSourceFiles { get; set; } = false;

        public string Status { get; private set; }
        public double MergeProgress { get; set; } = 0;
        public double MaxValue { get; set; } = 1;

        public ActionCommand StopExecutionCommand { get; set; }

        private CancellationTokenSource _tokenSource;

        public MergeJob()
        {
            StopExecutionCommand = new ActionCommand(new Action(CancelJob));
        }

        public Task ProcessJobAsync()
        {
            return Task.Run(() =>
            {
                ProcessJob();
            });
        }

        public void ProcessJob()
        {
            try
            {
                _tokenSource = new CancellationTokenSource();

                SetProgressBar(0, SourceFiles.Count());

                using (var output = new BinaryWriter(new StreamWriter(TargetFile).BaseStream))
                {
                    foreach (var file in SourceFiles)
                    {
                        if (_tokenSource.Token.IsCancellationRequested)
                            break;

                        Console.WriteLine(file.FullPath);

                        using (Mp3FileReader reader = new Mp3FileReader(file.FullPath))
                        {
                            if ((output.BaseStream.Position == 0) && (reader.Id3v2Tag != null))
                            {
                                output.Write(reader.Id3v2Tag.RawData, 0, reader.Id3v2Tag.RawData.Length);
                            }

                            Mp3Frame frame;
                            while ((frame = reader.ReadNextFrame()) != null)
                            {
                                output.Write(frame.RawData, 0, frame.RawData.Length);
                            }
                        }

                        MergeProgress++;

                        Application.Current.Dispatcher.Invoke(() =>
                        {
                            OnPropertyChanged(nameof(MergeProgress));
                        });
                    }
                }

                if (!_tokenSource.IsCancellationRequested)
                {
                    if (DeleteSourceFiles)
                        foreach (var file in SourceFiles)
                            File.Delete(file.FullPath);

                    //Invoke job finished callback
                    if (MergeJobFinished != null)
                        Application.Current.Dispatcher.Invoke(MergeJobFinished);
                }
                else
                {
                    Cleanup();
                }
            }
            catch (Exception)
            {
                SetStatus($"Merge job '{TargetName}' failed!");
                Cleanup();
            }
        }

        public void CancelJob()
        {
            _tokenSource.Cancel();

            SetStatus("Job canceled. Waiting for cleanup...");
        }
        
        private void SetProgressBar(double progress, double maxValue)
        {
            MaxValue = maxValue;
            MergeProgress = progress;

            Application.Current.Dispatcher.Invoke(() =>
            {
                OnPropertyChanged(nameof(MaxValue));
                OnPropertyChanged(nameof(MergeProgress));
            });
        }

        private void SetStatus(string status)
        {
            Status = status;

            Application.Current.Dispatcher.Invoke(() =>
            {
                OnPropertyChanged(nameof(Status));
            });
        }

        private void Cleanup()
        {
            if (File.Exists(TargetFile))
                File.Delete(TargetFile);

            SetProgressBar(0, 1);
        }
    }
}
