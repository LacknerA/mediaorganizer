﻿using NAudio.Wave;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MediaOrganizer.Components.Helper;

namespace MediaOrganizer.Modules
{
    public partial class MusicMergeControl : UserControl, INotifyPropertyChanged, ISelectionListener
    {
        #region INotifyPropertyChanged
        private void OnPropertyChanged(string propName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public FileBrowser FileBrowser { get; set; }
        public IEnumerable<DirectoryItem> SelectedFiles { get; set; }

        public MergeController MergeController { get; set; }

        public string OutputName { get; set; } = string.Empty;
        public string Status { get; set; }
        
        public bool DeleteFiles { get; set; } = false;
        public bool UserFolderName { get; set; } = true;

        public MusicMergeControl(FileBrowser fileBrowser)
        {
            FileBrowser = fileBrowser;

            MergeController = new MergeController();

            DataContext = this;
            InitializeComponent();
        }

        public void EnableListening()
        {
            FileBrowser.FileSelectionChanged += FileBrowser_FileSelectionChanged;
            UpdateSelection();
        }

        public void DisableListening()
        {
            FileBrowser.FileSelectionChanged -= FileBrowser_FileSelectionChanged;
        }

        private void CreateMergeJob()
        {
            if (SelectedFiles?.All(sf => sf.GetType() == typeof(FileObject) && sf.ItemName.GetFileExtension() == ".mp3") == true)
            {
                var dir = FileBrowser.DirectoryController.CurrentPath;
                var mergedFile = Path.Combine(dir, OutputName + ".mp3");

                if (OutputName != string.Empty && !File.Exists(mergedFile))
                {
                    var mergeJob = new MergeJob();

                    mergeJob.SourceFiles = new List<DirectoryItem>(SelectedFiles);
                    mergeJob.TargetFile = mergedFile;
                    mergeJob.DeleteSourceFiles = DeleteFiles;
                    mergeJob.MergeJobFinished += () =>
                    {
                        FileBrowser.DirectoryController.LoadDirectory(false);
                    };

                    MergeController.ExecuteJob(mergeJob);
                }
                else
                {
                    Status = "Please enter a valid file name (does the name already exists?)!";
                }
            }
            else
            {
                Status = "Please only select mp3 files!";
            }
        }
        
        private void UpdateSelection()
        {
            SelectedFiles = from file in FileBrowser.DirectoryController.DirectoryContent
                            where file.IsSelected
                            select file;

            if (SelectedFiles.All(sf => sf.GetType() == typeof(FileObject) && sf.ItemName.GetFileExtension() == ".mp3"))
            {
                var path = SelectedFiles.FirstOrDefault()?.FullPath;

                if (path != null)
                    OutputName = Directory.GetParent(path).Name;

                NotifyPropertiesChanged();
            }
        }

        private void FileBrowser_FileSelectionChanged()
        {
            UpdateSelection();
        }

        private void NotifyPropertiesChanged()
        {
            OnPropertyChanged(nameof(OutputName));
            OnPropertyChanged(nameof(Status));
        }

        #region UIEvents

        private void Merge_Click(object sender, RoutedEventArgs e)
        {
            CreateMergeJob();
        }

        #endregion

    }
}
