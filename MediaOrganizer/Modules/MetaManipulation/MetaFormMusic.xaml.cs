﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows;
using MediaOrganizer.Annotations;
using TagLib;
using File = TagLib.File;
using MediaOrganizer.Components.Helper;

namespace MediaOrganizer.Modules
{
    public partial class MetaFormMusic : MetaForm, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private struct MetaContainer
        {
            public DirectoryItem DirectoryItem { get; set; }
            public File MetaFile { get; set; }
        }

        public string Title { get; set; }
        public string Artist { get; set; }

        public IPicture AlbumCover { get; set; }

        public string Album { get; set; }
        public int Track { get; set; }
        public int TrackCount { get; set; }
        public string Comment { get; set; }

        public string Status { get; set; }

        private List<MetaContainer> _files;

        public MetaFormMusic()
        {
            DataContext = this;
            InitializeComponent();
        }

        public override void LoadFiles(IEnumerable<DirectoryItem> files)
        {
            //Create needed object format
            _files = files.Select(f => new MetaContainer()
            {
                DirectoryItem = f,
                MetaFile = TagLib.File.Create(f.FullPath)
            }).ToList();

            ShowMetaData();
        }

        private void ShowMetaData()
        {
            try
            {
                var meta = _files.FirstOrDefault();

                Title = meta.MetaFile.Tag.Title;
                Artist = meta.MetaFile.Tag.JoinedPerformers;
                Album = meta.MetaFile.Tag.Album;
                AlbumCover = meta.MetaFile.Tag.Pictures.FirstOrDefault();

                Track = (int)meta.MetaFile.Tag.Track;
                TrackCount = (int)meta.MetaFile.Tag.TrackCount;
                Comment = meta.MetaFile.Tag.Comment;

                Status = "";

                NotifyValuesChanged();
            }
            catch
            {
                // ignored
            }
        }

        private async void SaveMetaChanges()
        {
            SaveButton.ShowProgress();

            var trackStart = Track;

            try
            {
                await Task.Run(() =>
                {
                    foreach (var meta in _files)
                    {
                        /* Set title*/
                        if (_files.Count > 1)
                        {
                            var oldTitle = meta.MetaFile.Tag.Title;
                            var index = trackStart.ToString().PadLeft(TrackCount.ToString().Length, '0');

                            meta.MetaFile.Tag.Title = Title;
                            meta.MetaFile.Tag.Title = meta.MetaFile.Tag.Title.Replace("<index>", index);
                            meta.MetaFile.Tag.Title = meta.MetaFile.Tag.Title.Replace("<title>", oldTitle);
                            meta.MetaFile.Tag.Title = meta.MetaFile.Tag.Title.Replace("<file>", meta.DirectoryItem.ItemName.RemoveFileExtension());
                        }
                        else
                            meta.MetaFile.Tag.Title = Title;

                        meta.MetaFile.Tag.Performers = new[] { Artist };
                        meta.MetaFile.Tag.Album = Album;

                        if (AlbumCover != null)
                            meta.MetaFile.Tag.Pictures = new[] { AlbumCover };

                        meta.MetaFile.Tag.Track = (uint)trackStart++;
                        meta.MetaFile.Tag.TrackCount = (uint)TrackCount;
                        meta.MetaFile.Tag.Comment = Comment;

                        meta.MetaFile.Save();
                    }
                });
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not save new metadata!\n" + ex.Message);
            }

            Status = "Changes saved!";
            NotifyValuesChanged();
            
            SaveButton.HideProgress();
        }

        private async void ClearMetadata()
        {
            ClearButton.ShowProgress();

            await Task.Run(() =>
            {
                foreach (var meta in _files)
                {
                    meta.MetaFile.Tag.Clear();
                }
            });

            Status = "Metadata cleared!";
            ShowMetaData();

            ClearButton.HideProgress();
        }

        private async void ClearOtherMetadata()
        {
            ClearOtherButton.ShowProgress();

            await Task.Run(() =>
            {
                foreach (var meta in _files)
                {
                    meta.MetaFile.Tag.Clear();
                }
            });
            
            SaveMetaChanges();

            Status = "Cleared invisible tags!";

            ClearOtherButton.HideProgress();
        }

        private void NotifyValuesChanged()
        {
            OnPropertyChanged(nameof(Title));
            OnPropertyChanged(nameof(Artist));
            OnPropertyChanged(nameof(Album));
            OnPropertyChanged(nameof(Track));
            OnPropertyChanged(nameof(TrackCount));
            OnPropertyChanged(nameof(Comment));
            OnPropertyChanged(nameof(AlbumCover));

            OnPropertyChanged(nameof(Status));
        }

        private void BtnSave_Click(object sender, RoutedEventArgs e)
        {
            SaveMetaChanges();
        }

        private void BtnClearOther_Click(object sender, RoutedEventArgs e)
        {
            ClearOtherMetadata();
        }

        private void BtnClear_Click(object sender, RoutedEventArgs e)
        {
            ClearMetadata();
        }
    }
}
