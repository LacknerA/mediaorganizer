﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaOrganizer.Modules
{
    class MetaManipulation : ModuleBase
    {
        public MetaManipulation(FileBrowser dirCtrl) : base(dirCtrl)
        {
            ModuleName = "Meta Manipulation";
            Control = new MetaManipulationControl(dirCtrl);
            IconPath = "../../assets/metaManipulationIcon.png";
        }

        public override void ModuleSetActive()
        {
            var form = Control as ISelectionListener;

            if (form != null)
                form.EnableListening();
        }

        public override void ModuleSetInactive()
        {
            var form = Control as ISelectionListener;

            if (form != null)
                form.DisableListening();
        }
    }
}
