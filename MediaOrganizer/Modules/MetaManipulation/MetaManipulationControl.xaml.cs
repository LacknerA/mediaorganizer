﻿using MediaOrganizer.Components.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace MediaOrganizer.Modules
{
    public partial class MetaManipulationControl : UserControl, INotifyPropertyChanged, ISelectionListener
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        public FileBrowser FileBrowser { get; set; }
        public IEnumerable<DirectoryItem> SelectedFiles { get; set; }
        public MetaForm ActiveForm { get; set; }
        
        public Dictionary<string, MetaForm> MetaForms { get; set; } = new Dictionary<string, MetaForm>()
        {
            { "taglib/mp3", new MetaFormMusic() },
            { "taglib/mkv", null },
            { "taglib/avi", null }
        };

        public string ActiveMimeType { get; set; }

        public MetaManipulationControl(FileBrowser _dirCtrl)
        {
            FileBrowser = _dirCtrl;
            
            DataContext = this;           
            InitializeComponent();
        }
        
        public void EnableListening()
        {
            FileBrowser.FileSelectionChanged += FileBrowser_FileSelectionChanged;
            UpdateSelection();
        }        

        public void DisableListening()
        {
            FileBrowser.FileSelectionChanged -= FileBrowser_FileSelectionChanged;
        }

        private void LoadMetaForm()
        {            
            var selectedExt = SelectedFiles.FirstOrDefault()?.ItemName.GetFileExtension();

            //Check if all selected filetypes have the same extension
            if(selectedExt != null && SelectedFiles.All(i => i.ItemName.GetFileExtension() == selectedExt))
            {
                try
                {
                    var targetFile = TagLib.File.Create(SelectedFiles.FirstOrDefault()?.FullPath);                    
                    ActiveMimeType = targetFile.MimeType;
                    
                    ActiveForm = MetaForms[ActiveMimeType];
                    ActiveForm.LoadFiles(SelectedFiles);
                }
                catch
                {
                    // ignored
                }
            }
        }

        private void UpdateSelection()
        {
            SelectedFiles = from file in FileBrowser.DirectoryController.DirectoryContent
                            where file.IsSelected
                            select file;

            LoadMetaForm();
            
            NotifyValuesChanged();
        }
                
        private void NotifyValuesChanged()
        {
            OnPropertyChanged(nameof(ActiveMimeType));
            OnPropertyChanged(nameof(ActiveForm));
        }

        private void FileBrowser_FileSelectionChanged()
        {
            UpdateSelection();
        }
    }
}
