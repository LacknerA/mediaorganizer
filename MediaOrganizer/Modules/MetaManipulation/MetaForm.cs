﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MediaOrganizer.Modules
{
    public abstract class MetaForm : UserControl
    {
        public abstract void LoadFiles(IEnumerable<DirectoryItem> files);
    }
}
