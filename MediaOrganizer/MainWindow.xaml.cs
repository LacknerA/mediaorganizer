﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using System.IO;
using System.Collections.Generic;
using System.Windows.Controls;
using MediaOrganizer.Modules;
using System.Text;

namespace MediaOrganizer
{
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        public List<ModuleBase> Modules { get; set; }
        public ModuleBase ActiveModule { get; set; }

        public ActionCommand SearchCommand { get; set; } = new ActionCommand(() =>
        {
            FileBrowser.SelectedBrowser.ShowSearchBar();
        });

        public ActionCommand PasteCommand { get; set; } = new ActionCommand(() =>
        {
            FileBrowser.SelectedBrowser.PasteFiles();
        });

        public ActionCommand CutCommand { get; set; } = new ActionCommand(() =>
        {
            FileBrowser.SelectedBrowser.CutFiles();
        });

        public ActionCommand DeleteCommand { get; set; } = new ActionCommand(() =>
        {
            FileBrowser.SelectedBrowser.DeleteFiles();
        });

        public MainWindow()
        {
            Modules = new List<ModuleBase>();
            
            DataContext = this;
            InitializeComponent();

            RegisterModules();
            FileBrowser.ActivateBrowser();
        }

        private void RegisterModules()
        {
            Modules.Add(new FileManipulation(FileBrowser));
            Modules.Add(new MetaManipulation(FileBrowser));
            Modules.Add(new MusicMerge(FileBrowser));
            Modules.Add(new VideoConverter(FileBrowser));
            Modules.Add(new PodcastManager(FileBrowser));

            ActivateModule(Modules[0]);

            //Connect activation request events
            foreach (var module in Modules)
                module.ActivationRequest.ExecuteAction += OnModuleActivationRequest;

            OnPropertyChanged("Modules");
        }

        private void ActivateModule(ModuleBase module)
        {
            if (ActiveModule == module) return;

            if (ActiveModule != null)
                ActiveModule.IsActive = false;

            module.IsActive = true;
            ActiveModule = module;

            OnPropertyChanged("ActiveModule");
        }

        private void OnModuleActivationRequest(ModuleBase module)
        {
            ActivateModule(module);
        }

        #region UI Events
        private void PathChangedHandler(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
                if (FileBrowser.DirectoryController.CurrentPath == "")
                    FileBrowser.DirectoryController.LoadDrives();
                else
                    FileBrowser.DirectoryController.LoadDirectory();
        }

        private void FileBackButton_Click(object sender, RoutedEventArgs e)
        {
            FileBrowser.DirectoryController.GoBack();
        }

        private void FileForwardButton_Click(object sender, RoutedEventArgs e)
        {
            FileBrowser.DirectoryController.GoForward();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.XButton1)
                FileBrowser.SelectedBrowser.DirectoryController.GoBack();

            if (e.ChangedButton == MouseButton.XButton2)
                FileBrowser.SelectedBrowser.DirectoryController.GoForward();
        }
        #endregion
    }
}
