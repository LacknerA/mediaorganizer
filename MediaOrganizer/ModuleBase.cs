﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MediaOrganizer
{
    public abstract class ModuleBase : INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }

        #endregion

        public ClickCommand<ModuleBase> ActivationRequest { get; set; }
        public string ModuleName { get; set; }
        public string IconPath { get; set; }

        private UserControl _control;
        public UserControl Control
        {
            get
            {
                return _control;
            }
            set
            {
                _control = value;
                OnPropertyChanged(nameof(Control));
            }
        } 

        private bool _isActive;
        public bool IsActive
        {
            get
            {
                return _isActive;
            }
            set
            {
                _isActive = value;

                if (_isActive)
                    ModuleSetActive();
                else
                    ModuleSetInactive();

                OnPropertyChanged("IsActive");
            }
        }

        public FileBrowser FileBrowser { get; set; }

        protected ModuleBase(FileBrowser fileBrowser)
        {
            ActivationRequest = new ClickCommand<ModuleBase>(this);
            IsActive = false;

            FileBrowser = fileBrowser;
        }

        public virtual void ModuleSetActive() { }
        public virtual void ModuleSetInactive() { }
    }
}
