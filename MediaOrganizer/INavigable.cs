﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace MediaOrganizer
{
    public interface INavigable
    {
        void NavigateTo(UserControl ctrl);
        void NavigateBack();
    }
}
