﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaOrganizer
{
    interface ISelectionListener
    {
        void EnableListening();
        void DisableListening();
    }
}
