﻿using System;
using System.IO;
using System.Runtime.Serialization;

namespace MediaOrganizer
{
    public class PersistenceManager
    {
        public static bool WriteToXml<T>(T obj, string filePath)
        {
            try
            {
                using (var fStream = File.Open(filePath, FileMode.Create))
                {
                    var ser = new DataContractSerializer(typeof(T));
                    ser.WriteObject(fStream, obj);
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static T ReadFromXml<T>(string filePath)
        {
            try
            {
                T obj;

                using (var fStream = File.Open(filePath, FileMode.Open))
                {
                    var ser = new DataContractSerializer(typeof(T));
                    obj = (T)ser.ReadObject(fStream);
                }

                return obj;
            }
            catch (Exception)
            {
                return default(T);
            }
        }
    }
}
