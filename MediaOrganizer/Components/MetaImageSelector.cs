﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Controls.Primitives;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using TagLib;

using Button = System.Windows.Controls.Button;
using Control = System.Windows.Controls.Control;
using Image = System.Windows.Controls.Image;

namespace MediaOrganizer.Components
{
    [TemplatePart(Name = "PART_Image", Type = typeof(Image))]
    [TemplatePart(Name = "PART_ZoomButton", Type = typeof(Image))]
    [TemplatePart(Name = "PART_ImageSelector", Type = typeof(Button))]
    [TemplatePart(Name = "PART_LocalImage", Type = typeof(Button))]
    [TemplatePart(Name = "PART_ZoomPopup", Type = typeof(Popup))]
    public class MetaImageSelector : Control, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region DependencyProperties
        public IPicture SourceImage
        {
            get
            {
                return (IPicture)GetValue(SourceImageProperty);
            }
            set
            {
                SetValue(SourceImageProperty, value);
            }
        }

        public static readonly DependencyProperty SourceImageProperty =
            DependencyProperty.Register("SourceImage", typeof(IPicture), typeof(MetaImageSelector), new PropertyMetadata(null, OnSourceImageChanged));

        private static void OnSourceImageChanged(DependencyObject d, DependencyPropertyChangedEventArgs e) => (d as MetaImageSelector)?.OnPropertyChanged(nameof(DisplayImage));
        #endregion

        public BitmapImage DisplayImage
        {
            get
            {
                if (SourceImage == null)
                    return GetDefaultImage();

                return LoadImage(SourceImage.Data.Data);
            }
        }

        private Image _image;
        private Image _zoomButton;
        private Button _imageSelector;
        private Button _localImage;
        private Popup _zoomPopup;

        static MetaImageSelector()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(MetaImageSelector), new FrameworkPropertyMetadata(typeof(MetaImageSelector)));
        }

        public override void OnApplyTemplate()
        {
            _image = GetTemplateChild("PART_Image") as Image;
            _zoomButton = GetTemplateChild("PART_ZoomButton") as Image;
            _imageSelector = GetTemplateChild("PART_ImageSelector") as Button;
            _localImage = GetTemplateChild("PART_LocalImage") as Button;
            _zoomPopup = GetTemplateChild("PART_ZoomPopup") as Popup;

            //Select local
            if (_localImage != null)
            {
                _localImage.Click += (sender, e) =>
                {
                    SelectLocalImage();
                };
            }

            //Select from internet
            if (_imageSelector != null)
            {
                _imageSelector.Click += (sender, e) =>
                {
                    //ToDo: Implement internet selection
                };
            }

            //Connect zoom events
            if (_zoomButton != null)
            {
                _zoomButton.MouseEnter += (sender, e) =>
                {
                    if(_zoomPopup != null && SourceImage != null)
                        _zoomPopup.IsOpen = true;
                };

                _zoomButton.MouseLeave += (sender, e) =>
                {
                    if(_zoomPopup != null)
                        _zoomPopup.IsOpen = false;
                };
            }

            base.OnApplyTemplate();
        }

        private void SelectLocalImage()
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.Filter = "jpg (*.jpg)|*.jpg|png (*.png)|*.png|All Files (*.*)|*.*";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                SourceImage =
                    new TagLib.Picture(
                        new TagLib.ByteVector(
                            (byte[])
                                new System.Drawing.ImageConverter().ConvertTo(
                                    System.Drawing.Image.FromFile(fileDialog.FileName), typeof (byte[]))));
            }
        }

        private static BitmapImage LoadImage(byte[] imageData)
        {
            if (imageData == null || imageData.Length == 0) return null;

            var image = new BitmapImage();
            using (var mem = new MemoryStream(imageData))
            {
                mem.Position = 0;
                image.BeginInit();
                image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = null;
                image.StreamSource = mem;
                image.EndInit();
            }

            image.Freeze();
            return image;
        }

        private static BitmapImage GetDefaultImage()
        {
            var bmp = new Bitmap(180, 180);

            using (var graph = Graphics.FromImage(bmp))
            {
                Rectangle imageSize = new Rectangle(0, 0, 180, 180);
                graph.FillRectangle(System.Drawing.Brushes.LightGray, imageSize);
            }
            return LoadImage(ImageToByte(bmp));
        }

        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

    }
}
