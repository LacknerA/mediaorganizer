﻿using System;
using System.Windows.Input;

namespace MediaOrganizer
{
    public class ClickCommand<T> : ICommand
    {
        public event Action<T> ExecuteAction;
        public T Sender { get; set; }
        public event EventHandler CanExecuteChanged
        {
            add { }

            remove { }
        }

        public ClickCommand(T sender)
        {
            Sender = sender;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            if (ExecuteAction != null)
                ExecuteAction(Sender);
        }
    }
}