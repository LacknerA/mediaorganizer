﻿using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Controls;

namespace MediaOrganizer.Components
{
    [TemplatePart(Name = "PART_DefaultContent", Type = typeof(ContentPresenter))]
    [TemplatePart(Name = "PART_ProgressRing", Type = typeof(ProgressRing))]
    public class ProgressButton : Button
    {
        private ContentPresenter _contentPresenter;
        private ProgressRing _progressRing;

        static ProgressButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ProgressButton), new FrameworkPropertyMetadata(typeof(ProgressButton)));
        }

        public override void OnApplyTemplate()
        {
            _contentPresenter = GetTemplateChild("PART_DefaultContent") as ContentPresenter;
            _progressRing = GetTemplateChild("PART_ProgressRing") as ProgressRing;
            
            base.OnApplyTemplate();
        }

        public void ShowProgress()
        {
            _contentPresenter.Visibility = Visibility.Collapsed;
            _progressRing.Visibility = Visibility.Visible;
        }

        public void HideProgress()
        {
            _contentPresenter.Visibility = Visibility.Visible;
            _progressRing.Visibility = Visibility.Collapsed;
        }        
    }
}
