﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace MediaOrganizer.Components.Helper
{
    public static class StringHelper
    {
        public static string RemoveFileExtension(this String str)
        {
            var extension = Path.GetExtension(str);

            if (extension != null)
            {
                int extLength = extension.Length;

                if (extLength > 0)
                    return str.Remove(str.Length - extLength);
            }

            return str;
        }

        public static string GetFileName(this String str)
        {
            return Path.GetFileName(str);
        }

        public static string GetFileExtension(this String str)
        {
            return Path.GetExtension(str);
        }

        public static string ExtractPath(this String str)
        {
            return Path.GetDirectoryName(str);
        }

        public static string FirstCharUpper(this String str)
        {
            if (String.IsNullOrEmpty(str)) throw new ArgumentException(nameof(str));
            return str.First().ToString().ToUpper() + str.Substring(1);
        }

        public static string WildcardToRegex(this String pattern)
        {
            return "^" + Regex.Escape(pattern)
                              .Replace(@"\*", ".*")
                              .Replace(@"\?", ".")
                       + "$";
        }

        public static string ToValidPath(this string str)
        {
            return str?.Replace( '\\','/');
        }

        public static string ToUrl(this string str)
        {
            return HttpUtility.UrlPathEncode(str);
        }
    }
}
