﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace MediaOrganizer.Components
{
    [TemplatePart(Name = "PART_Input", Type = typeof(TextBox))]
    [TemplatePart(Name = "PART_ValueUp", Type = typeof(Button))]
    [TemplatePart(Name = "PART_ValueDown", Type = typeof(Button))]
    public class NumberSelector : Control, INotifyPropertyChanged
    {
        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region DependencyProperties
        public int Value
        {
            get { return (int)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); OnPropertyChanged(nameof(Value)); RaiseValueChangedEvent(); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(int), typeof(NumberSelector));
        #endregion

        #region Events
        public static readonly RoutedEvent ValueChangedEvent = EventManager.RegisterRoutedEvent(
                "ValueChanged",
                RoutingStrategy.Bubble,
                typeof(RoutedEventHandler),
                typeof(NumberSelector));

        public event RoutedEventHandler ValueChanged
        {
            add { AddHandler(ValueChangedEvent, value); }
            remove { RemoveHandler(ValueChangedEvent, value); }
        }
        
        private void RaiseValueChangedEvent()
        {
            RoutedEventArgs newEventArgs = new RoutedEventArgs(ValueChangedEvent);
            RaiseEvent(newEventArgs);
        }
        #endregion

        static NumberSelector()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(NumberSelector), new FrameworkPropertyMetadata(typeof(NumberSelector)));
        }

        public override void OnApplyTemplate()
        {
            var btnUp = GetTemplateChild("PART_ValueUp") as Button;
            var btnDown = GetTemplateChild("PART_ValueDown") as Button;

            if (btnUp != null)
                btnUp.Click += ValueUp_Click;

            if (btnDown != null)
                btnDown.Click += ValueDown_Click;

            base.OnApplyTemplate();
        }

        private void ValueUp_Click(object sender, RoutedEventArgs e)
        {
            Value++;
        }

        private void ValueDown_Click(object sender, RoutedEventArgs e)
        {
            Value--;
        }
    }
}
