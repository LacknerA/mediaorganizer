﻿using MediaOrganizer.Components.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace MediaOrganizer.Components
{

    [TemplatePart(Name = "PART_Display", Type = typeof(TextBlock))]
    [TemplatePart(Name = "PART_Edit", Type = typeof(TextBox))]
    class EditLabel : Control, INotifyPropertyChanged
    {
        private enum Mode
        {
            Display,
            Edit
        }

        #region NotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region DepenencyProperties
        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }
            set
            {
                SetValue(TextProperty, value);
                OnPropertyChanged("Text");
            }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(EditLabel));

        public ActionWrapper RequestEditCallback
        {
            get
            {
                return (ActionWrapper)GetValue(RequestEditCallbackProperty);
            }
            set
            {
                SetValue(RequestEditCallbackProperty, value);
            }
        }

        public static readonly DependencyProperty RequestEditCallbackProperty =
            DependencyProperty.Register("RequestEditCallback", typeof(ActionWrapper), typeof(EditLabel), new PropertyMetadata(new PropertyChangedCallback(EditCallbackChanged)));

        public ICommand BeginEditCommand
        {
            get { return (ICommand)GetValue(BeginEditCommandProperty); }
            set { SetValue(BeginEditCommandProperty, value); }
        }

        public static readonly DependencyProperty BeginEditCommandProperty =
            DependencyProperty.Register("BeginEditCommand", typeof(ICommand), typeof(EditLabel));

        public ICommand EndEditCommand
        {
            get { return (ICommand)GetValue(EndEditCommandProperty); }
            set { SetValue(EndEditCommandProperty, value); }
        }

        public static readonly DependencyProperty EndEditCommandProperty =
            DependencyProperty.Register("EndEditCommand", typeof(ICommand), typeof(EditLabel));
        #endregion

        private Mode _mode = Mode.Display;
        private string _textBuffer = "";

        private TextBlock _displayControl;
        private TextBox _editControl;

        static EditLabel()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(EditLabel), new FrameworkPropertyMetadata(typeof(EditLabel)));
        }

        private static void EditCallbackChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var wrapper = d.GetValue(args.Property) as ActionWrapper;
            wrapper.Action = new Action((d as EditLabel).BeginEdit);
        }

        public override void OnApplyTemplate()
        {
            _displayControl = GetTemplateChild("PART_Display") as TextBlock;
            _editControl = GetTemplateChild("PART_Edit") as TextBox;

            _displayControl.MouseLeftButtonDown += DisplayControl_LeftMouseDown;

            if (_editControl != null)
            {
                _editControl.LostFocus += EditControl_LostFocus;
                _editControl.KeyDown += EditControl_KeyDown;
            }
            
            base.OnApplyTemplate();
        }

        private void EditControl_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter || e.Key == Key.Escape)
            {
                if (e.Key == Key.Escape)
                    Text = _textBuffer;

                EndEdit();
            }
        }

        private void DisplayControl_LeftMouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (e.ClickCount > 1)
                BeginEdit();
        }

        private void EditControl_LostFocus(object sender, RoutedEventArgs e)
        {
            EndEdit();
        }

        public void BeginEdit()
        {
            BeginEditCommand?.Execute(null);

            _textBuffer = Text;
            _mode = Mode.Edit;
            RefreshState();

            Dispatcher.BeginInvoke(DispatcherPriority.Input, new System.Action(delegate ()
            {
                _editControl.Focus();
                _editControl.SelectAll();
                Keyboard.Focus(_editControl);
            }));
        }

        public void EndEdit()
        {
            if (_mode == Mode.Edit)
            {
                _mode = Mode.Display;
                Keyboard.ClearFocus();
                RefreshState();

                EndEditCommand?.Execute(Text);
            }
        }

        private void RefreshState()
        {
            switch (_mode)
            {
                case Mode.Display:
                    VisualStateManager.GoToState(this, "DefaultState", false);
                    break;
                case Mode.Edit:
                    VisualStateManager.GoToState(this, "EditState", false);
                    break;
                default:
                    break;
            }
        }

    }
}
