﻿using System;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using MediaOrganizer.Components.Helper;

namespace MediaOrganizer
{
    public partial class FileBrowser : UserControl, INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion

        public static FileBrowser SelectedBrowser;
        public event Action FileSelectionChanged;

        public DirectoryController DirectoryController { get; set; }
        public DirectoryItem SelectedDirectoryItem { get; set; }
        public string SearchPattern { get; set; }

        public Visibility SearchBarVisibility { get; set; } = Visibility.Collapsed;

        public SolidColorBrush BrowserBackground
        {
            get
            {
                if (Equals(SelectedBrowser, this))
                    return new SolidColorBrush(Colors.White);

                return new SolidColorBrush(Colors.WhiteSmoke);
            }
        }

        public FileBrowser()
        {
            DirectoryController = new DirectoryController();
            DirectoryController.LoadDrives();

            SelectedBrowser = this;

            InitializeComponent();
        }

        public void RenameFile()
        {
            var item = DirectoryController.DirectoryContent.FirstOrDefault(di => di.IsSelected && di.AllowRename);
            item?.RequestRename();
        }

        public void PasteFiles()
        {
            if (Directory.Exists(DirectoryController.CurrentPath))
            {
                Mouse.OverrideCursor = Cursors.Wait;

                foreach (var item in BrowserClipboard.Clipboard)
                {
                    if (File.Exists(item.FullPath))
                        File.Move(item.FullPath, Path.Combine(DirectoryController.CurrentPath, item.ItemName));
                }

                DirectoryController.LoadDirectory(false);
                Mouse.OverrideCursor = Cursors.Arrow;
            }
        }

        public void CutFiles()
        {
            BrowserClipboard.Clipboard = DirectoryController.DirectoryContent.ToList();
        }

        public void DeleteFiles()
        {
            var selectedItems = DirectoryController.DirectoryContent.Where(di => di.IsSelected && di.AllowDelete);

            if (selectedItems.Count() > 0)
            {
                var res = MessageBox.Show("Do you really want to delete " + selectedItems.Count() + " item(s)?", "Delete files", MessageBoxButton.YesNo, MessageBoxImage.Question);

                if (res == MessageBoxResult.Yes)
                {
                    var itemBuffer = selectedItems.ToArray();
                    foreach (var item in itemBuffer)
                        item.DeleteItem();

                    DirectoryController.LoadDirectory(false);
                }
            }
        }

        public void CreateFolder()
        {
            if (Directory.Exists(DirectoryController.CurrentPath))
            {
                int index = 1;
                string folderName = "New Folder";

                //Get free name
                while (Directory.Exists(Path.Combine(DirectoryController.CurrentPath, folderName)))
                {
                    folderName = "New Folder (" + (index++) + ")";
                }

                //Create directory
                Directory.CreateDirectory(Path.Combine(DirectoryController.CurrentPath, folderName));
                DirectoryController.LoadDirectory(false);
            }
        }

        public void ShowSearchBar()
        {
            SearchPattern = "";
            SearchBarVisibility = Visibility.Visible;

            OnPropertyChanged(nameof(SearchBarVisibility));
            OnPropertyChanged(nameof(SearchPattern));
        }

        public void HideSearchBar()
        {
            DirectoryController.LoadDirectory(false);

            SearchBarVisibility = Visibility.Collapsed;
            OnPropertyChanged(nameof(SearchBarVisibility));
        }

        public void ActivateBrowser()
        {
            var lastActive = SelectedBrowser;
            SelectedBrowser = this;

            lastActive.OnPropertyChanged(nameof(BrowserBackground));
            OnPropertyChanged(nameof(BrowserBackground));
        }

        private void OnSelectionChanged()
        {
            FileSelectionChanged?.Invoke();
        }

        #region UIEvents
        private void DirectoryItem_Clicked(object sender, MouseButtonEventArgs e)
        {
            if (SelectedDirectoryItem != null && SelectedDirectoryItem.Navigatable)
            {
                DirectoryController.CurrentPath = SelectedDirectoryItem.FullPath;
                DirectoryController.LoadDirectory();
            }
        }

        private void DirectoryItem_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F2)
            {
                RenameFile();
            }
        }

        private void FileView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OnSelectionChanged();
        }

        private void FileView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.F5)
                DirectoryController.LoadDirectory(false);
        }

        private void ContextItemRename_Click(object sender, RoutedEventArgs e)
        {
            RenameFile();
        }

        private void ContextItemPaste_Click(object sender, RoutedEventArgs e)
        {
            PasteFiles();
        }

        private void ContextItemCut_Click(object sender, RoutedEventArgs e)
        {
            CutFiles();
        }

        private void ContextItemDelete_Click(object sender, RoutedEventArgs e)
        {
            DeleteFiles();
        }

        private void ContextItemCreateFolder_Click(object sender, RoutedEventArgs e)
        {
            CreateFolder();
        }

        private void FileBrowser_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            ActivateBrowser();
        }

        private void HideSearchBar_Click(object sender, RoutedEventArgs e)
        {
            HideSearchBar();
        }

        private void SearchBox_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
                DirectoryController.SearchFiles(SearchPattern);

            if (e.Key == Key.Escape)
                HideSearchBar();
        }

        private void SearchButton_Click(object sender, RoutedEventArgs e)
        {
            DirectoryController.SearchFiles(SearchPattern);
        }
        #endregion

        private T FindVisualChild<T>(DependencyObject obj) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is T)
                    return (T)child;
                else
                {
                    T childOfChild = FindVisualChild<T>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }
    }
}
