﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Input;
using MediaOrganizer.Components.Helper;

namespace MediaOrganizer
{
    public class DirectoryController : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(String property)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(property));
        }
        #endregion

        enum Mode
        {
            Explorer,
            Search
        }

        public string CurrentPath { get; set; }
        public ObservableCollection<DirectoryItem> DirectoryContent { get; set; }
        public Stack<string> PathHistory;
        public Stack<string> ForwardHistory;

        private Mode _controllerMode = Mode.Explorer;
        private string _searchRoot;

        public DirectoryController()
        {
            DirectoryContent = new ObservableCollection<DirectoryItem>();
            PathHistory = new Stack<string>();
            ForwardHistory = new Stack<string>();
        }

        public void LoadDrives()
        {
            Mouse.OverrideCursor = Cursors.Wait;
            CurrentPath = "";
            DirectoryContent.Clear();

            var getDriveName = new Func<DriveInfo, string>((DriveInfo dirInf) =>
            {
                try{
                    return dirInf.VolumeLabel.FirstCharUpper();
                }
                catch{
                    return "";
                }
            });

            foreach (var drive in DriveInfo.GetDrives())
                DirectoryContent.Add(new DriveObject()
                {
                    ItemName = getDriveName(drive) + " (" + drive.Name + ")",
                    FullPath = drive.Name,
                    ImagePath = "../assets/driveIcon.png",
                    Navigatable = true
                });

            OnPropertyChanged("CurrentPath");
            Mouse.OverrideCursor = Cursors.Arrow;
        }

        public void LoadDirectory(bool clearStack = true)
        {
            if (_controllerMode == Mode.Search){
                _controllerMode = Mode.Explorer;
                CurrentPath = _searchRoot;
            }

            if (Directory.Exists(CurrentPath))
            {
                Mouse.OverrideCursor = Cursors.Wait;
                var dirInf = new DirectoryInfo(CurrentPath);

                DirectoryContent.Clear();

                foreach (var dir in dirInf.GetDirectories().OrderBy(x => x.ToString(), new NaturalSortComparator()))
                    if ((dir.Attributes & FileAttributes.Hidden) == 0)
                        DirectoryContent.Add(new DirectoryObject()
                        {
                            ItemName = dir.Name,
                            FullPath = dir.FullName,
                            Icon = IconReader.GetFolderIcon(dir.FullName, IconReader.IconSize.Small, IconReader.FolderType.Open),
                            Navigatable = true,
                            RefreshCallback = LoadDirectory
                        });

                foreach (var file in dirInf.GetFiles().OrderBy(x => x.ToString(), new NaturalSortComparator()))
                    if ((file.Attributes & FileAttributes.Hidden) == 0)
                        DirectoryContent.Add(new FileObject()
                        {
                            ItemName = file.Name,
                            FullPath = file.FullName,
                            Icon = IconReader.GetFileIcon(file.FullName, IconReader.IconSize.Small, false),
                            Navigatable = false,
                            RefreshCallback = LoadDirectory
                        });

                if (clearStack)
                    ForwardHistory.Clear();

                //Save path to history stack
                if (PathHistory.Count == 0 || PathHistory.Peek() != CurrentPath)
                    PathHistory.Push(CurrentPath);

                OnPropertyChanged(nameof(CurrentPath));
                Mouse.OverrideCursor = Cursors.Arrow;
            }
        }

        public void SearchFiles(string searchPattern)
        {
            if (_controllerMode == Mode.Explorer)
            {
                if (!Directory.Exists(CurrentPath))
                    return;

                _controllerMode = Mode.Search;
                _searchRoot = CurrentPath;
                CurrentPath = "";
            }

            Mouse.OverrideCursor = Cursors.Wait;

            DirectoryContent.Clear();
            var searchResults = GetMatches(_searchRoot, searchPattern);

            if (searchResults.Count > 0)
            {
                foreach (var file in searchResults.OrderBy(x => x.ToString(), new NaturalSortComparator()))
                {
                    DirectoryContent.Add(new FileObject()
                    {
                        ItemName = file.Name,
                        FullPath = file.FullName,
                        Icon = IconReader.GetFileIcon(file.FullName, IconReader.IconSize.Small, false),
                        Navigatable = false,
                        AllowRename = false
                    });
                }
            }
            else
            {
                DirectoryContent.Add(new DisplayObject { ItemName = "No results found!", IsSelectable = false});
            }

            Mouse.OverrideCursor = Cursors.Arrow;
            OnPropertyChanged(nameof(CurrentPath));
        }
        
        public void GoForward()
        {
            if (ForwardHistory.Count > 0)
            {
                CurrentPath = ForwardHistory.Pop();
                LoadDirectory(false);
            }
        }

        public void GoBack()
        {
            if (PathHistory.Count > 1)
            {
                //Remove current directory
                ForwardHistory.Push(PathHistory.Pop());

                //Get and load last directory
                CurrentPath = PathHistory.Pop();
                LoadDirectory(false);
            }
            else
            {
                ForwardHistory.Clear();
                PathHistory.Clear();

                CurrentPath = "";
                LoadDrives();
            }
        }

        public List<FileInfo> GetMatches(string path, string pattern)
        {
            var files = new List<FileInfo>();
            var dirInf = new DirectoryInfo(path);

            foreach (var dir in dirInf.GetDirectories())
                files.AddRange(GetMatches(dir.FullName, pattern));
            
            files.AddRange(dirInf.GetFiles().Where(file => Regex.IsMatch(file.Name, pattern.WildcardToRegex())));

            return files;
        }
    }
}
