﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MediaOrganizer.Components.Helper;

namespace MediaOrganizer
{
    public class DirectoryObject : DirectoryItem
    {
        public DirectoryObject()
        {
            AllowRename = true;
            AllowDelete = true;
        }

        public override void RequestRename()
        {
            RenameRequestDelegate.Action?.Invoke();
        }

        public override void RenameItem(string name)
        {
            if (name != ItemName)
            {
                var origin = FullPath;
                FullPath = Path.Combine(FullPath.ExtractPath(), name);

                if (!Directory.Exists(FullPath))
                {
                    Directory.Move(origin, FullPath);
                }
                else
                {
                    MessageBox.Show("The file '" + name + "' already exists!", "Renaming error", MessageBoxButton.OK, MessageBoxImage.Error);
                    FullPath = origin;
                }

                RefreshCallback?.Invoke(false);
                OnPropertyChanged(nameof(ItemName));
            }
        }

        public override void DeleteItem()
        {
            try
            {
                Directory.Delete(FullPath, true);
                RefreshCallback?.Invoke(false);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not delete directory! " + ex.Message);
            }
        }

        public override bool Exists(string path)
        {
            return Directory.Exists(path);
        }

        public override void Move(string sourceFile, string destinationFile)
        {
            Directory.Move(sourceFile, destinationFile);
        }
    }
}
