﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using MediaOrganizer.Components.Helper;

namespace MediaOrganizer
{
    public class FileObject : DirectoryItem
    {
        public FileObject()
        {
            AllowRename = true;
            AllowDelete = true;
        }

        public override void RequestRename()
        {
            RenameRequestDelegate.Action?.Invoke();
        }

        public override void RenameItem(string name)
        {
            if (name != ItemName)
            {
                var origin = FullPath;
                FullPath = Path.Combine(FullPath.ExtractPath(), name);

                if (!File.Exists(FullPath))
                {
                    File.Move(origin, FullPath);
                }
                else
                {
                    MessageBox.Show("The file '" + name + "' already exists!", "Renaming error", MessageBoxButton.OK, MessageBoxImage.Error);
                    FullPath = origin;
                }

                RefreshCallback?.Invoke(false);
                OnPropertyChanged(nameof(ItemName));
            }
        }

        public override void DeleteItem()
        {
            File.Delete(FullPath);
            RefreshCallback?.Invoke(false);
        }

        public override bool Exists(string path)
        {
            return File.Exists(path);
        }

        public override void Move(string sourceFile, string destinationFile)
        {
            File.Move(sourceFile, destinationFile);
        }
    }
}
