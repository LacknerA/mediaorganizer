﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MediaOrganizer
{
    public class DriveObject : DirectoryItem
    {
        public DriveObject()
        {
            AllowRename = false;
            AllowDelete = false;
        }        
    }
}
