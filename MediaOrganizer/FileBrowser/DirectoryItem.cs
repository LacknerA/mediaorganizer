﻿using MediaOrganizer.Components.Helper;
using System;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace MediaOrganizer
{
    public class DirectoryItem : INotifyPropertyChanged
    {
        #region INotifyPropertyChanged
        protected void OnPropertyChanged(string on)
        {
            if (PropertyChanged == null) return;
            PropertyChanged(this, new PropertyChangedEventArgs(on));
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public delegate void RefreshCallbackDelegate(bool clearStack);

        public string ItemName { get; set; }
        public string FullPath { get; set; }

        public Icon Icon { get; set; }
        public string ImagePath { get; set; }
        public ImageSource Image
        {
            get
            {
                //Icon is set
                if (Icon != null)
                {
                    return Imaging.CreateBitmapSourceFromHIcon(
                            Icon.Handle,
                            Int32Rect.Empty,
                            BitmapSizeOptions.FromEmptyOptions());
                }
                //Image path is set
                else if (ImagePath != null)
                {
                    return new BitmapImage(new Uri(ImagePath, UriKind.Relative));
                }
                else
                {
                    return null;
                }
            }
        }

        public bool Navigatable { get; set; }
        public bool IsSelected { get; set; }
        public bool AllowRename { get; set; }
        public bool AllowDelete { get; set; }
        public bool IsSelectable { get; set; } = true;
        
        public ActionWrapper RenameRequestDelegate { get; set; }
        public ActionCommand<string> RenameCommand { get; set; }
        public RefreshCallbackDelegate RefreshCallback { get; set; }
        
        public DirectoryItem()
        {
            RenameCommand = new ActionCommand<string>(RenameItem);
            RenameRequestDelegate = new ActionWrapper();
        }

        public virtual void RequestRename() { }
        public virtual void RenameItem(string name) { ItemName = name; RefreshCallback?.Invoke(false); }
        public virtual void DeleteItem() { }

        public virtual bool Exists(string path) { return false; }
        public virtual void Move(string sourceFile, string destinationFile) { }
        
        public override string ToString()
        {
            return ItemName;
        }
    }
}